from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, NewItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    to_do_items = TodoItem.objects.all()
    context = {
        "todo_list_list": todos,
        "all_items": to_do_items
    }
    return render(request, 'todos/todos.html', context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    to_do_items = TodoItem.objects.all()
    context = {
        "this_list": list,
        "all_items": to_do_items
    }

    return render(request, 'todos/todo_list_detail.html', context)


def todo_list_create(request):
    if request.method != "POST":
        form = TodoForm()
    else:
        form = TodoForm(request.POST)
        if form.is_valid():
            new_list = form
            item = form.save()
            new_list.save()

            return redirect("todo_list_detail", id=item.id)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    old_form = get_object_or_404(TodoList, id=id)
    if request.method != "POST":
        form = TodoForm()
    else:
        form = TodoForm(request.POST, instance=old_form)
        if form.is_valid():
            item = form.save()
            form.save()
            return redirect("todo_list_detail", id)

    context = {
        "old_form": old_form,
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    old_form = TodoList.objects.get(id=id)
    if request.method == "POST":
        old_form.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    if request.method != "POST":
        form = NewItemForm()
    else:
        form = NewItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            form.save()
            return redirect("todo_list_detail", id=item.list.id)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    old_form = get_object_or_404(TodoItem, id=id)
    if request.method != "POST":
        form = NewItemForm()
    else:
        form = NewItemForm(request.POST, instance=old_form)
        if form.is_valid():
            item = form.save()
            form.save()
            return redirect("todo_list_detail", id=item.list.id)

    context = {
        "old_form": old_form,
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)
